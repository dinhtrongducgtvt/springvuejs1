package com.testapi.springbootandvue.models;



import lombok.Data;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Objects;

@Entity
@Table(name = "products")
public class Product {
    @Id
    //    @GeneratedValue(strategy = GenerationType.AUTO)
        // sequence : tạo quy tắc khi thêm mới bản ghi ( thay thế cho @GeneratedValue)
        // ví dụ quy tắc tạo id tăng 1
    @SequenceGenerator(
            name = "product_sequence",
            sequenceName = "product_sequence",
            allocationSize = 1 // increment by 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "product_sequence"
    )
    private  long id;
    @Column(name = "name", nullable = false, unique = true,length = 300)
    private String productName;
    @Column(name = "_year")
    private int year;
    @Column(name = "price")
    private double price;
    @Column(name = "url")
    private String url;

    // @Transient là các trường ko đc lưu trong DB , là các trường đc tính toán từ các thuộc tính khác
    @Transient
    private  int age;

    public int getAge() {
        return Calendar.getInstance().get(Calendar.YEAR) - year;
    }

    public Product(String productName, int year, double price, String url) {

        this.productName = productName;
        this.year = year;
        this.price = price;
        this.url = url;
    }

    public Product() {

    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }

    public String getUrl() {
        return url;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id
                && year == product.year
                && Double.compare(product.price, price) == 0 && age == product.age
                && Objects.equals(productName, product.productName)
                && Objects.equals(url, product.url);
    }


}
