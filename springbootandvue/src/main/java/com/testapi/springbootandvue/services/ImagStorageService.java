package com.testapi.springbootandvue.services;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class ImagStorageService implements IStorageService {
    // thu muc upload anh
    private final Path storageFolder = Paths.get("uploads");

    // neu chua co thu muc "uploads" no se dc tao ra 1 lan duy nhat
    // contructor
    public ImagStorageService() {
        try {
            Files.createDirectories(storageFolder);
        } catch (IOException exception) {
            throw new RuntimeException("Can't init storage", exception);
        }
    }


    @Override
    public String storeFile(MultipartFile file) {
        try {

        }catch (){

        }
        return null;
    }

    @Override
    public Stream<Path> loadAll() {
        return null;
    }

    @Override
    public byte readFileContent(String fileName) {
        return 0;
    }

    @Override
    public void deleteFile() {

    }
}
