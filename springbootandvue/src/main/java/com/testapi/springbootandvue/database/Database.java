package com.testapi.springbootandvue.database;

import com.testapi.springbootandvue.models.Product;
import com.testapi.springbootandvue.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Database implements  CommandLineRunner {

    @Autowired
    ProductRepository productRepository;

    /// theem data cho H2 DATABASE
    @Override
    public void run(String... args) throws Exception {
        Product product1 = new Product("Phone",2000,180000.0,"");
        Product product2 = new Product("Android",2020,150000.0,"");
        System.out.println("Insert data : " + productRepository.save(product1));
        System.out.println("Insert data : " + productRepository.save(product2));

    }
}
