package com.testapi.springbootandvue.controller;


import com.testapi.springbootandvue.models.Product;
import com.testapi.springbootandvue.models.ResponeObject;
import com.testapi.springbootandvue.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * The type Product controller.
 */
@RestController
@RequestMapping(path = "/api/products")
public class ProductController {

    // co che DI
    @Autowired
    private ProductRepository productRepository;

    /**
     * Gets all products.
     *
     * @return the all products
     */
    @GetMapping("")
    List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    /**
     * Find by id response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @GetMapping("/{id}")
    ResponseEntity<ResponeObject> findById(@PathVariable long id) {
        Optional foundProduct = productRepository.findById(id);
        if (foundProduct.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponeObject("OK", "Query product successfully", foundProduct)
            );
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponeObject("failed", "Cannot find product with id = " + id, "")
            );
        }

    }

    /**
     * Insert product response entity.
     *
     * @param product the product
     * @return the response entity
     */
// insert
    @PostMapping("/insertProduct")
    ResponseEntity<ResponeObject> insertProduct(@RequestBody Product product) {
        List<Product> foundProducts = productRepository.findProductByProductName(product.getProductName().trim());
        if (foundProducts.size() > 0) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                    new ResponeObject("failed", "ProductName existed !", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponeObject("OK", "Insert suucessfully", productRepository.save(product))
        );
    }

    /**
     * Update product response entity.
     *
     * @param newproduct the newproduct
     * @param id         the id
     * @return the response entity
     */
// insert - update : update if found , otherwise insert
    @PutMapping("/{id}")
    ResponseEntity<ResponeObject> updateProduct(@RequestBody Product newproduct , @PathVariable long id){
        Product updateProduct = productRepository.findById(id)
                          .map(product -> {
                              // update if found
                                product.setProductName(newproduct.getProductName());
                                product.setUrl(newproduct.getUrl());
                                product.setYear(newproduct.getYear());
                                product.setPrice(newproduct.getPrice());
                                return productRepository.save(product);
                          }).orElseGet(() -> {
                              // otherwise insert
                                return productRepository.save(newproduct);
                });
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponeObject("OK", "Update suucessfully", updateProduct));
    }

    /**
     * Delete product response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/{id}")
    ResponseEntity<ResponeObject> deleteProduct(@PathVariable long id){
        boolean exists = productRepository.existsById(id);
        if(exists){
            productRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponeObject("OK", "Delete suucessfully ProductId : " + id, ""));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponeObject("FAILED", "Cannot find product with id : " + id, ""));
    }






}
