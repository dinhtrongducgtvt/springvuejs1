//package com.testapi.springbootandvue.service;
//
//import com.testapi.springbootandvue.models.Product;
//
//import java.util.List;
//
//public interface ProductService {
//    Product createProduct(Product product);
//
//    Product updateProduct(Product product);
//
//    List<Product> getAllProduct();
//
//    Product getProductById(long id);
//
//    void deleteProduct(long id);
//}
