//package com.testapi.springbootandvue.service;
//
//import com.testapi.springbootandvue.exeption.ResourceNotFoundException;
//import com.testapi.springbootandvue.models.Product;
//import com.testapi.springbootandvue.repo.ProductRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.Optional;
//
//@Service
//public class ProductServiceImpl implements  ProductService{
//    @Autowired
//    private ProductRepository productRepository;
//
//    @Override
//    public Product createProduct(Product product) {
//        return productRepository.save(product);
//    }
//
//    @Override
//    public Product updateProduct(Product product) {
//        Optional<Product> productDB = productRepository.findById(product.getId());
//        if(productDB.isPresent()){
//            Product productUpdate = productDB.get();
//            productUpdate.setId(product.getId());
//            productUpdate.setProductName(product.getProductName());
//            productUpdate.setPrice(product.getPrice());
//            productUpdate.setYear(product.getYear());
//            productUpdate.setUrl(product.getUrl());
//            return productUpdate;
//        } else {
//            throw new ResourceNotFoundException("Record not found with id = " + product.getId());
//        }
//
//    }
//
//    @Override
//    public List<Product> getAllProduct() {
//        return null;
//    }
//
//    @Override
//    public Product getProductById(long id) {
//        return null;
//    }
//
//    @Override
//    public void deleteProduct(long id) {
//
//    }
//}
